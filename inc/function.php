<?php
function SCpesan(){
	if(isset($_SESSION['pesan'])){
		$ssp = explode('|',$_SESSION['pesan']);
		echo "<script>pesan('$ssp[0]','$ssp[1]','$ssp[2]');</script>";
		unset($_SESSION['pesan']);
	}
}

function notif($pesan,$type='success'){
	$str = '<div class="alert alert-'.$type.'">'.$pesan.'</div>';
	echo "<script>$(document).ready(function(e){
				$('".$str."').appendTo('body').insertBefore('.login-box');
			});
		  </script>";
}
function ob_html_compress($buf){
    return preg_replace(['/\>[^\S ]+/s','/[^\S ]+\</s','/(\s)+/s','/<!--(.|\s)*?-->/'],['>','<','\\1'],$buf);
}

function anti_injection($data){
  $filter = mysql_real_escape_string(stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
  return $filter;
}

function ceklevel($level){
	$username = $_SESSION['username'];
	$b=mysql_fetch_array(mysql_query("select level from users where username = '$username'"));
	$a = explode(';',$b[0]);
	$c = count($a);
	$tampil=false;
	for($i=0;$i<$c;$i++){
		if($level==$a[$i]){
			$tampil = true;
		}
	}
	//echo $a[1];
	return $tampil;
}
function zipBanyak($src,$zipname, $with_src) {
	$zip = new ZipArchive;
	$zip->open($zipname);
    $dir = opendir($src); 
    while( false !== ($file = readdir($dir))){ 
        if( $file != '.' && $file != '..' ) { 
            if(is_dir($src.'/'.$file)){ 
                zipBanyak($src.'/'.$file,$zipname, $with_src); 
            } else { 
				if ($with_src==true){
              		$zip->addFile($src.'/'.$file,$src.'/'.$file);
				}else{
					$zip->addFile($file,$file);
				}
			} 
        } 
    } 
    closedir($dir);
	$zip->close();
}
function timeago($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'tahun',
        'm' => 'bulan',
        'w' => 'minggu',
        'd' => 'hari',
        'h' => 'jam',
        'i' => 'menit',
        's' => 'detik',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
			if($v=='detik'){
				$v='beberapa detik';
			}else{
					$v = ($diff->$k > 1 ? 'Sekitar ' : '').$diff->$k . ' ' . $v;
			}
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' yg lalu' : 'baru saja';
}

function getConfig($key){
	$qsettings=mysql_query("select config_value from config where config_key='$key'") or die(mysql_error());
	$bsettings=mysql_fetch_array($qsettings);
	return $bsettings[0];
}

function setConfig($key,$val){
	$qsettings=mysql_query("update config set config_value ='$val' where config_key='$key'") or die(mysql_error());
}

function hapusTempCart(){
	$qtemp=mysql_query("select * from cart_temp where order_id='0'") or die(mysql_error());
	while($btemp=mysql_fetch_array($qtemp)){
		$hitung = ceil((time()-$btemp['waktu'])/60/60);
		if($hitung>=48){
			$qdel=mysql_query("delete from temp_pengiriman where ID_session='$btemp[ID_session]' and order_id='0';") or die(mysql_error());
			$qdel=mysql_query("delete from cart_temp where ID_session='$btemp[ID_session]' and order_id='0';") or die(mysql_error());	
		}
	} 
}

function filterPost(){
	foreach($_POST as $key => $val){
		$_POST[$key] = mysql_real_escape_string($val);
	}
}

function filterGet(){
	foreach($_GET as $key => $val){
		$_GET[$key] = mysql_real_escape_string($val);
	}	
}

function Random(){
	return strtoupper(substr(md5(time()),mt_rand(0,25),5));
}

function Password($password){
	return hash('sha256',$password.'7F2991158C2899256E1A347ED2E39646');
}

function getDataURI($image, $mime = '') {
	return 'data: '.(function_exists('mime_content_type') ? mime_content_type($image) : $mime).';base64,'.base64_encode(file_get_contents($image));
}

function getToken(){
	$token = md5(rand().microtime());
	$token_count=count($_SESSION['token']);
	$_SESSION['token'][$token_count] = $token;
	return $token;
}

function cekToken($sToken){
	if(in_array($sToken,$_SESSION['token'])){
		unset($_SESSION['token']);
		return true;
	}else{
		unset($_SESSION['token']);
		return false;
	}
}

function importSQL($SQLfile){
	$query = '';
	$sqlScript = file($SQLfile);
	foreach ($sqlScript as $line)	{
		$startWith = substr(trim($line), 0 ,2);
		$endWith = substr(trim($line), -1 ,1);
		if (empty($line) || $startWith == '--' || $startWith == '/*' || $startWith == '//') {
			continue;
		}
		$query = $query . $line;
		if ($endWith == ';') {
			mysql_query($query) or die('<div class="error-response sql-import-response">Problem in executing the SQL query <b>' . $query. '</b></div>');
			$query= '';		
		}
	}
}



