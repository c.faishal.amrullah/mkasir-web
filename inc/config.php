<?php
date_default_timezone_set('Asia/Jakarta');
filterPost();
filterGet();
if ($_SERVER['REQUEST_METHOD'] =='POST'){
	if(isset($_POST['token'])){
		if(!cekToken($_POST['token'])){
			$ref = $_SERVER['HTTP_REFERER'];
			header("location: $ref");
		}
	}
}
