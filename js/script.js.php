<?php
error_reporting(0);
require_once('../inc/minify.php');
$seconds_to_cache = 604800;
$ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
ob_start("ob_gzhandler");
ob_start("fn_minify_js_union");
header('Content-type: text/javascript; charset="utf-8"', true);
header("Pragma: cache");
header("Cache-Control: max-age=$seconds_to_cache");
header("Expires: $ts");
// load css files
readfile("../plugins/jquery/jquery.min.js"); 
readfile("../plugins/bootstrap/js/bootstrap.min.js"); 
readfile("../plugins/jquery-datatable/jquery.dataTables.js"); 
readfile("../plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.min.js"); 
readfile("../plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"); 
readfile("../plugins/jquery-datatable/extensions/export/buttons.flash.min.js"); 
readfile("../plugins/jquery-datatable/extensions/export/jszip.min.js"); 
readfile("../plugins/jquery-datatable/extensions/export/pdfmake.min.js"); 
readfile("../plugins/jquery-datatable/extensions/export/vfs_fonts.js"); 
readfile("../plugins/jquery-datatable/extensions/export/buttons.html5.min.js"); 
readfile("../plugins/jquery-datatable/extensions/export/buttons.print.min.js"); 
readfile("../plugins/jquery-slimscroll/jquery.slimscroll.js"); 
readfile("../plugins/node-waves/waves.min.js"); 
readfile("../plugins/jquery-validation/jquery.validate.js"); 
readfile("../plugins/sweetalert/sweetalert.min.js"); 
readfile("../plugins/bootstrap-notify/bootstrap-notify.min.js"); 
readfile("../plugins/jquery-validation/jquery.validate.js"); 
readfile("../plugins/momentjs/moment.js"); 
readfile("../plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"); 
readfile("../plugins/summernote/summernote.min.js"); 
readfile("../plugins/select2/js/select2.min.js");
readfile("../plugins/file-input/file-input.min.js");
readfile("admin.js"); 
readfile("script.js"); 
readfile("form_ajax.js");
//
ob_end_flush();
?>