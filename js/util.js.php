<?php
require_once('../inc/dbcon.php');
require_once('../inc/function.php');
require_once('../inc/config.php');
header('Content-type: text/javascript');
?>
$(document).ready(function(e) {
	loadPage();
});
function noClick(){
	$("a").click(function(e) {
			e.preventDefault();
			var hr = $(this).attr('href');
			if(hr!='javascript:void(0);' || hr!='#'){
				history.pushState(null, null, hr);
                loadPage();
			}
	});
}
function cekHash(){
	if($('body').hasClass('overlay-open')){
		$('.overlay').fadeOut();
		$('body').removeClass('overlay-open');
    }
	loadPage();
}
function loadPage(){
	var p = getUrlVars()[0];
	$("li[class='active']").removeAttr('class');
	if(typeof(p)=='undefined' || p=='http:'){
		$("a[href='<?php echo getConfig('base_url');?>#home']").closest('li').addClass('active');
		$.ajax({url:'<?php echo getConfig('base_url');?>route_ajax.php',
				data:{page:'home'},
				type:'GET',
				beforeSend: function(a){
					$('.page-loader-wrapper').show();
				},
				success: function(msg){
					$("#hasil").html(msg);
				}
		}).done(function(){
        	$('.page-loader-wrapper').fadeOut();
            initComponent();
        });
	}else{
    	var lht = getUrlVars()['lihat'];
        var edt = getUrlVars()['edit'];
        var hps = getUrlVars()['hapus'];
        var add = getUrlVars()['add'];
		$("a[href='<?php echo getConfig('base_url');?>#"+p+"']").closest('li').addClass('active');
		$.ajax({url:'<?php echo getConfig('base_url');?>route_ajax.php',
				data:{page:p,lihat:lht,edit:edt,hapus:hps,add:add},
				type:'GET',
				beforeSend: function(a){
					$('.page-loader-wrapper').show();
				},
				success: function(msg){
					$("#hasil").html(msg);
				}
		}).done(function(){
        	$('.page-loader-wrapper').fadeOut();
            initComponent();
        });
	}
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('#') + 1).split('/');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function notif(pesan){
	$.notify({message: pesan},
    		 {allow_dismiss: true,
              newest_on_top: true,
              timer:1500,
              delay:1500,
              placement: {from: 'top',align: 'right'},
              animate: {enter:'animated bounceInRight',exit:'animated bounceOutRight'}
             });
}
function initComponent() {
    $('.back').click(function(e) {
    	self.history.back();
	});
    $.AdminBSB.input.activate();
    $.AdminBSB.select.activate();
    $.AdminBSB.table.activate();
    $.AdminBSB.DatePicker.activate();
    $.AdminBSB.summerNote.activate();
    initAjax();
}

function validateImage(elmt){
	var val = $(elmt).val();
    var reader = new FileReader();
    var fdata = $(elmt)[0].files[0];
    switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase()){
        case 'gif': case 'jpg': case 'png':
        	reader.onload = function(e) {
				$('#fdata').val(reader.result);
			}
            reader.readAsDataURL(fdata);
            $('#fname').val(fdata.name);
            break;
        default:
            $(elmt).val('');
            break;
    }
}