<?php
session_start();
require_once('inc/dbcon.php');
require_once('inc/function.php');
require_once('inc/config.php');
$now=date('Y-m-d H:i:s');
header('Content-type: text/plain');
$page=$_GET['page'];
$edit=$_GET['edit'];
$lihat=$_GET['lihat'];
$hapus=$_GET['hapus'];
$add=$_GET['add'];
if($_SESSION['blog_login']){
	ob_start("ob_html_compress");
	if(is_file('pages/'.$page.'.php')){
		require_once('pages/'.$page.'.php');
	}else{
		require_once('pages/home.php');
	}
	ob_end_flush();
}
?>