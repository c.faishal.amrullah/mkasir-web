<?php
error_reporting(E_ALL);
require_once('../inc/minify.php');
$seconds_to_cache = 604800;
$ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
ob_start("ob_gzhandler");
ob_start("minify_css");
header('Content-type: text/css; charset="utf-8"', true);
header("Pragma: cache");
header("Cache-Control: max-age=$seconds_to_cache");
header("Expires: $ts");

// load css files
readfile("../plugins/google-font/font.css");
readfile("../plugins/material-icon/material-icon.css");
readfile("../plugins/bootstrap/css/bootstrap.min.css");
readfile("../plugins/node-waves/waves.min.css");
readfile("../plugins/animate-css/animate.min.css");
readfile("../plugins/sweetalert/sweetalert.css");
readfile("../plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.min.css");
readfile("../plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css");
readfile("../plugins/summernote/summernote.css");
readfile("../plugins/select2/css/select2.min.css");
readfile("../plugins/file-input/file-input.min.css");
readfile("materialize.css");
readfile("style.min.css");
//
ob_end_flush();
?>