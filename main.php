<?php
if(isset($_GET['logout']) || !$_SESSION['blog_login']){
	session_destroy();
	header('location: '.getConfig('base_url'));
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<title>mKasir</title>
<link rel="icon" href="<?php echo getConfig('base_url');?>images/icon.png" type="image/x-icon">
<link href="<?php echo getConfig('base_url');?>css/style.css.php" rel="stylesheet" type="text/css">
<link href="<?php echo getConfig('base_url');?>css/themes.min.css" rel="stylesheet" type="text/css">
<script src="<?php echo getConfig('base_url');?>plugins/jquery/jquery.min.js"></script>
<script src="<?php echo getConfig('base_url');?>plugins/bootstrap/js/bootstrap.min.js"></script>
</head>
<body class="theme-blue" onhashchange="cekHash();">
<div class="page-loader-wrapper">
  <div class="loader">
    <div class="preloader">
      <div class="spinner-layer pl-white">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div>
        <div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>
    </div>
    <p style="color:white;">Tunggu...</p>
  </div>
</div>
<div class="overlay"></div>
<div class="search-bar">
  <div class="search-icon"> <i class="material-icons">search</i> </div>
  <input type="text" placeholder="START TYPING...">
  <div class="close-search"> <i class="material-icons">close</i> </div>
</div>
<nav class="navbar">
  <div class="container-fluid">
    <div class="navbar-header"> <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a> <a href="javascript:void(0);" class="bars"></a> <a class="navbar-brand" href="<?php echo getConfig('base_url');?>?page=home">mKasir <small>Backend Management</small></a> </div>
    <div class="collapse navbar-collapse" id="navbar-collapse">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>
        <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"> <i class="material-icons">notifications</i> <span class="label-count">7</span> </a>
          <ul class="dropdown-menu">
            <li class="header">NOTIFICATIONS</li>
            <li class="body">
              <ul class="menu">
                <li> <a href="javascript:void(0);">
                  <div class="icon-circle bg-light-green"> <i class="material-icons">person_add</i> </div>
                  <div class="menu-info">
                    <h4>12 new members joined</h4>
                    <p> <i class="material-icons">access_time</i> 14 mins ago </p>
                  </div>
                  </a> </li>
                <li> <a href="javascript:void(0);">
                  <div class="icon-circle bg-cyan"> <i class="material-icons">add_shopping_cart</i> </div>
                  <div class="menu-info">
                    <h4>4 sales made</h4>
                    <p> <i class="material-icons">access_time</i> 22 mins ago </p>
                  </div>
                  </a> </li>
                <li> <a href="javascript:void(0);">
                  <div class="icon-circle bg-red"> <i class="material-icons">delete_forever</i> </div>
                  <div class="menu-info">
                    <h4><b>Nancy Doe</b> deleted account</h4>
                    <p> <i class="material-icons">access_time</i> 3 hours ago </p>
                  </div>
                  </a> </li>
                <li> <a href="javascript:void(0);">
                  <div class="icon-circle bg-blue"> <i class="material-icons">mode_edit</i> </div>
                  <div class="menu-info">
                    <h4><b>Nancy</b> changed name</h4>
                    <p> <i class="material-icons">access_time</i> 2 hours ago </p>
                  </div>
                  </a> </li>
                <li> <a href="javascript:void(0);">
                  <div class="icon-circle bg-blue-grey"> <i class="material-icons">comment</i> </div>
                  <div class="menu-info">
                    <h4><b>John</b> commented your post</h4>
                    <p> <i class="material-icons">access_time</i> 4 hours ago </p>
                  </div>
                  </a> </li>
                <li> <a href="javascript:void(0);">
                  <div class="icon-circle bg-light-green"> <i class="material-icons">cached</i> </div>
                  <div class="menu-info">
                    <h4><b>John</b> updated status</h4>
                    <p> <i class="material-icons">access_time</i> 3 hours ago </p>
                  </div>
                  </a> </li>
                <li> <a href="javascript:void(0);">
                  <div class="icon-circle bg-purple"> <i class="material-icons">settings</i> </div>
                  <div class="menu-info">
                    <h4>Settings updated</h4>
                    <p> <i class="material-icons">access_time</i> Yesterday </p>
                  </div>
                  </a> </li>
              </ul>
            </li>
            <li class="footer"> <a href="javascript:void(0);">View All Notifications</a> </li>
          </ul>
        </li>
        <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"> <i class="material-icons">flag</i> <span class="label-count">9</span> </a>
          <ul class="dropdown-menu">
            <li class="header">TASKS</li>
            <li class="body">
              <ul class="menu tasks">
                <li> <a href="javascript:void(0);">
                  <h4> Footer display issue <small>32%</small> </h4>
                  <div class="progress">
                    <div class="progress-bar bg-pink" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 32%"> </div>
                  </div>
                  </a> </li>
                <li> <a href="javascript:void(0);">
                  <h4> Make new buttons <small>45%</small> </h4>
                  <div class="progress">
                    <div class="progress-bar bg-cyan" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 45%"> </div>
                  </div>
                  </a> </li>
                <li> <a href="javascript:void(0);">
                  <h4> Create new dashboard <small>54%</small> </h4>
                  <div class="progress">
                    <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 54%"> </div>
                  </div>
                  </a> </li>
                <li> <a href="javascript:void(0);">
                  <h4> Solve transition issue <small>65%</small> </h4>
                  <div class="progress">
                    <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 65%"> </div>
                  </div>
                  </a> </li>
                <li> <a href="javascript:void(0);">
                  <h4> Answer GitHub questions <small>92%</small> </h4>
                  <div class="progress">
                    <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 92%"> </div>
                  </div>
                  </a> </li>
              </ul>
            </li>
            <li class="footer"> <a href="javascript:void(0);">View All Tasks</a> </li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<section>
  <aside id="leftsidebar" class="sidebar">
    <div class="user-info">
      <div class="image"> <img src="images/user.png" width="48" height="48" alt="User" /> </div>
      <div class="info-container">
        <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['blog_nama'];?></div>
        <div class="email"><?php echo $_SESSION['blog_email'];?></div>
        <div class="btn-group user-helper-dropdown"> <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
          <ul class="dropdown-menu pull-right">
            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
            <li role="seperator" class="divider"></li>
            <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
            <li role="seperator" class="divider"></li>
            <li><a href="<?php echo getConfig('base_url');?>?logout"><i class="material-icons">input</i>Logout</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="menu">
      <ul class="list">
        <li class="active"> <a href="<?php echo getConfig('base_url');?>#home"> <i class="material-icons">home</i> <span>Home</span> </a> </li>
        <li> <a href="<?php echo getConfig('base_url');?>#transaksi"> <i class="material-icons">attach_money</i> <span>Transaksi</span> </a> </li>
        <li> <a href="<?php echo getConfig('base_url');?>#posting"> <i class="material-icons">playlist_add_check</i> <span>Produk</span> </a> </li>
        <li> <a href="<?php echo getConfig('base_url');?>#kategori-posting"> <i class="material-icons">low_priority</i> <span>Kategori Produk</span> </a> </li>
        <li> <a href="<?php echo getConfig('base_url');?>#settings"> <i class="material-icons">settings</i> <span>Settings</span> </a> </li>
        <li> <a href="<?php echo getConfig('base_url');?>#user-login"> <i class="material-icons">person</i> <span>User Login</span> </a></li>
      </ul>
    </div>
  </aside>
</section>
<section class="content">
  <div class="container-fluid" id="hasil"></div>
</section>
<script src="<?php echo getConfig('base_url');?>plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?php echo getConfig('base_url');?>plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo getConfig('base_url');?>plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?php echo getConfig('base_url');?>plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?php echo getConfig('base_url');?>plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?php echo getConfig('base_url');?>plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?php echo getConfig('base_url');?>plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?php echo getConfig('base_url');?>plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?php echo getConfig('base_url');?>plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
<script src="<?php echo getConfig('base_url');?>plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
<script src="<?php echo getConfig('base_url');?>plugins/node-waves/waves.min.js"></script>
<script src="<?php echo getConfig('base_url');?>plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?php echo getConfig('base_url');?>plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo getConfig('base_url');?>plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
<script src="<?php echo getConfig('base_url');?>plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?php echo getConfig('base_url');?>plugins/momentjs/moment.js"></script>
<script src="<?php echo getConfig('base_url');?>plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<script src="<?php echo getConfig('base_url');?>plugins/summernote/summernote.min.js"></script>
<script src="<?php echo getConfig('base_url');?>plugins/select2/js/select2.min.js"></script>
<script src="<?php echo getConfig('base_url');?>plugins/file-input/file-input.min.js"></script>
<script src="<?php echo getConfig('base_url');?>js/admin.js"></script>
<script src="<?php echo getConfig('base_url');?>js/script.js"></script>
<script src="<?php echo getConfig('base_url');?>js/form_ajax.js"></script>
<script src="<?php echo getConfig('base_url');?>js/util.js.php"></script>
<script>
</script>
</body>
</html>