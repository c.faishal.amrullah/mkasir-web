<?php if($lihat){?>
<?php
$q=mysql_query("select * from produk_kategori where ID ='$lihat'") or die(mysql_error());
$b=mysql_fetch_array($q);
?>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>LIHAT KETEGORI PRODUK</h2>
      </div>
      <div class="body">
        <form>
        <input type="hidden" name="p" value="kategori-posting">
        <input type="hidden" name="t" value="update">
        <input type="hidden" name="token" value="<?php echo getToken();?>">
        <input type="hidden" name="id" value="<?php echo $b['ID'];?>">
          <div class="form-group">
            <label>Nama Kategori</label>
            <div class="form-line">
              <input type="text" name="nama_kategori" value="<?php echo $b['nama_kategori'];?>" class="form-control" readonly>
            </div>
          </div>
        <div class="form-group">
            <label>Posisi</label>
            <div class="form-line">
              <input type="number" min="0" name="posisi_kategori" value="<?php echo $b['posisi_kategori'];?>" class="form-control" readonly>
            </div>
        </div>
        <div class="form-group">
          <button type="button" class="btn btn-link bg-blue waves-effect back">KEMBALI</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
<?php }elseif($edit){?>
<?php
$q=mysql_query("select * from produk_kategori where ID ='$edit'") or die(mysql_error());
$b=mysql_fetch_array($q);
?>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>EDIT KETEGORI PRODUK</h2>
      </div>
      <div class="body">
        <form class="form_ajax">
        <input type="hidden" name="p" value="kategori-posting">
        <input type="hidden" name="t" value="update">
        <input type="hidden" name="token" value="<?php echo getToken();?>">
        <input type="hidden" name="id" value="<?php echo $b['ID'];?>">
          <div class="form-group">
            <label>Nama Kategori</label>
            <div class="form-line">
              <input type="text" name="nama_kategori" value="<?php echo $b['nama_kategori'];?>" class="form-control" required>
            </div>
        </div>
        <div class="form-group">
            <label>Posisi</label>
            <div class="form-line">
              <input type="number" min="0" name="posisi_kategori" value="<?php echo $b['posisi_kategori'];?>" class="form-control" required>
            </div>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-link bg-deep-purple waves-effect">SIMPAN</button>
          <button type="reset" class="btn btn-link bg-blue waves-effect back">BATAL</button>
        <div class="preloader pl-size-xs pull-right" style="display:none;" id="loading">
            <div class="spinner-layer pl-deep-purple">
              <div class="circle-clipper left">
                <div class="circle"></div>
              </div>
              <div class="circle-clipper right">
                <div class="circle"></div>
              </div>
            </div>
          </div>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
<?php }else{?>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>KETEGORI PRODUK</h2>
      </div>
      <div class="body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-hover dataTable js-exportable">
            <thead>
              <tr>
                <th>#</th>
                <th>Nama Kategori</th>
                <th>Posisi</th>
                <th>Tgl Buat</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
            	<?php
					$no=0;
					$q=mysql_query("select * from produk_kategori order by ID desc") or die(mysql_error());
					while($b=mysql_fetch_array($q)){
						$no++;
				?>
                		<tr>
                        	<td align="center"><?php echo $no;?></td>
                            <td><?php echo $b['nama_kategori'];?></td>
                            <td><?php echo $b['posisi_kategori'];?></td>
                            <td><?php echo $b['tgl_buat'];?></td>
                            <td nowrap align="center">
								<a href="<?php echo getConfig('base_url');?>#kategori-posting/lihat=<?php echo $b['ID'];?>" class="btn bg-deep-purple waves-effect" title="Lihat"><i class="material-icons">list</i></a>
                                <a href="<?php echo getConfig('base_url');?>#kategori-posting/edit=<?php echo $b['ID'];?>" class="btn bg-blue waves-effect" title="Edit"><i class="material-icons">edit</i></a>
                                <button class="btn bg-red waves-effect hapus_ajax" data-id="<?php echo $b['ID'];?>" data-p="kategori-posting" data-t="delete" data-token="<?php echo getToken();?>" title="Hapus"><i class="material-icons">delete</i></button>
                            </td>
                        </tr>
                <?php
					}
				?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<button class="btn btn-circle-lg bg-deep-purple waves-effect waves-circle waves-float frm btn-float" data-toggle="modal" data-target="#defaultModal" data-controls-modal="#defaultModal" data-backdrop="static" data-keyboard="false" data-tooltip="true" data-placement="top" title="Tambah Data"><i class="material-icons">add</i></button>
<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="defaultModalLabel">Tambah Kategori Posting</h4>
      </div>
      <form class="form_ajax">
        <input type="hidden" name="p" value="kategori-posting">
        <input type="hidden" name="t" value="create">
        <input type="hidden" name="token" value="<?php echo getToken();?>">
        <div class="modal-body">
          <div class="input-group">
            <label>Nama Kategori</label>
            <div class="form-line">
              <input type="text" name="nama_kategori" class="form-control" required>
            </div>
          </div>
        <div class="form-group">
            <label>Posisi</label>
            <div class="form-line">
              <input type="number" min="0" name="posisi_kategori" class="form-control" required>
            </div>
        </div>
        </div>
        <div class="modal-footer">
          <div class="preloader pl-size-xs pull-left" style="display:none" id="loading">
            <div class="spinner-layer pl-deep-purple">
              <div class="circle-clipper left">
                <div class="circle"></div>
              </div>
              <div class="circle-clipper right">
                <div class="circle"></div>
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-link bg-deep-purple waves-effect">SIMPAN</button>
          <button type="reset" class="btn btn-link bg-blue waves-effect" data-dismiss="modal">BATAL</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php } ?>
