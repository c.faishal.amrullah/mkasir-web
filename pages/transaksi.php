<?php if($lihat){?>

<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>LIHAT TRANSAKSI</h2>
      </div>
      <div class="body">
        <?php
		  	$q=mysql_query("select * from transaksi where ID='$lihat'") or die(mysql_error());
			$b=mysql_fetch_array($q);
		  ?>
        <div class="text-right">ID : <?php echo $b['ID'];?></div>
        <div class="text-right">Waktu : <?php echo $b['waktu'];?></div>
        <table class="ms table" width="100%">
        	<thead>
            	<tr style="text-align:center" class="text-center">
                	<th align="center">NO</th><th>ITEM</th><th>HARGA</th><th>QTY</th><th>JUMLAH</th>
                </tr>
            </thead>
            <tbody>
            	<?php 
				$str = $b['itemdata'];
				$js = (json_decode($str));
				for($i=0;$i<count($js->bcart);$i++){
					echo "<tr><td>".($i+1)."</td><td>".$js->bcart[$i]->nama."</td>
							  <td align='right'>".$js->bcart[$i]->harga."</td><td align='right'>".$js->bcart[$i]->qty."</td><td align='right'>".$js->bcart[$i]->jml."</td></tr>";
				}
				?>
                <tr style="font-weight:bold;">
                	<td colspan="3" align="right">SUBTOTAL</td><td>:</td><td align="right"><?php echo $b['subtotal'];?></td>
                </tr>
                <tr style="font-weight:bold;">
                	<td colspan="3" align="right">DISKON</td><td>:</td><td align="right"><?php echo $b['diskon'];?>%</td>
                </tr>
                <tr style="font-weight:bold;">
                	<td colspan="3" align="right">GRANDTOTAL</td><td>:</td><td align="right"><?php echo number_format($b['grandtotal']);?></td>
                </tr>
                <tr style="font-weight:bold;">
                	<td colspan="3" align="right">TUNAI</td><td>:</td><td align="right"><?php echo $b['bayar'];?></td>
                </tr>
                <tr style="font-weight:bold;">
                	<td colspan="3" align="right">KEMBALI</td><td>:</td><td align="right"><?php echo $b['kembali'];?></td>
                </tr>
            </tbody>
        </table>
        <button type="button" class="btn btn-link bg-blue waves-effect back" data-dismiss="modal">KEMBALI</button>
      </div>
    </div>
  </div>
</div>
<?php }else{?>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>TRANSAKSI</h2>
      </div>
      <div class="body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-hover dataTable js-exportable">
            <thead>
              <tr>
                <th>#</th>
                <th>ID</th>
                <th>Subtotal</th>
                <th>Diskon(%)</th>
                <th>GrandTotal</th>
                <th>Status</th>
                <th>Waktu</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php
					$no=0;
					$q=mysql_query("select * from transaksi a order by a.id_ desc") or die(mysql_error());
					while($b=mysql_fetch_array($q)){
						$no++;
				?>
              <tr>
                <td align="center"><?php echo $no;?></td>
                <td><?php echo $b['ID'];?></td>
                <td align="right"><?php echo $b['subtotal'];?></td>
                <td align="right"><?php echo $b['diskon'];?></td>
                <td align="right"><?php echo number_format($b['grandtotal']);?></td>
                <td><?php echo $b['status'];?></td>
                <td><?php echo $b['waktu'];?></td>
                <td nowrap align="center"><a href="<?php echo getConfig('base_url');?>#transaksi/lihat=<?php echo $b['ID'];?>" class="btn bg-deep-purple waves-effect" title="Lihat"><i class="material-icons">list</i></a>
                  <button class="btn bg-red waves-effect hapus_ajax" data-id="<?php echo $b['ID'];?>" data-p="transaksi" data-t="delete" data-token="<?php echo getToken();?>" title="Hapus"><i class="material-icons">delete</i></button></td>
              </tr>
              <?php
					}
				?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>
