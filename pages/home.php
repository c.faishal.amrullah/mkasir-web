﻿<div class="block-header">
  <h2>DASHBOARD</h2>
</div>
<div class="row clearfix">
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="info-box bg-pink">
      <div class="icon"> <a href="<?php echo getConfig('base_url');?>#posting"><i class="material-icons">playlist_add_check</i></a></div>
      <div class="content">
        <div class="text">PRODUK</div>
        <?php $j=mysql_num_rows(mysql_query("select * from produk")); ?>
        <div class="number count-to" data-from="0" data-to="<?php echo $j?>" data-speed="5" data-fresh-interval="20"></div>
      </div>
    </div>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="info-box bg-cyan hover-expand-effect">
      <div class="icon"> <a href="<?php echo getConfig('base_url');?>#kategori-posting"><i class="material-icons">low_priority</i></a></div>
      <div class="content">
        <div class="text">KATEGORI PRODUK</div>
        <?php $j=mysql_num_rows(mysql_query("select * from produk_kategori")); ?>
        <div class="number count-to" data-from="0" data-to="<?php echo $j;?>" data-speed="5" data-fresh-interval="20"></div>
      </div>
    </div>
  </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="info-box bg-light-blue hover-expand-effect">
      <div class="icon"><a href="<?php echo getConfig('base_url');?>#transaksi"><i class="material-icons">attach_money</i></a> </div>
      <div class="content">
        <div class="text">TRANSAKSI</div>
        <?php $j=mysql_num_rows(mysql_query("select * from transaksi")); ?>
        <div class="number count-to" data-from="0" data-to="<?php echo $j;?>" data-speed="5" data-fresh-interval="20"></div>
      </div>
    </div>
  </div>
</div>
<div class="alert alert-warning"><strong>Akun Demo!</strong><br>Semua data akan di reset seletah 1 jam.</div>
<script src="<?php echo getConfig('base_url');?>plugins/jquery-countto/jquery.countTo.js"></script> 
<script>
$(function () {
    $('.count-to').countTo();
});
</script>