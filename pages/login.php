<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<title>Login | mKasir - Backend Management</title>
<!-- Favicon-->
<link rel="icon" href="<?php echo getConfig('base_url');?>images/icon.png" type="image/x-icon">
<!-- Google Fonts -->
<link href="<?php echo getConfig('base_url');?>css/style.css.php" rel="stylesheet" type="text/css">
<!-- Jquery Core Js -->
<script src="<?php echo getConfig('base_url');?>plugins/jquery/jquery.min.js"></script>
</head>
<body class="login-page theme-blue">
<div class="page-loader-wrapper">
  <div class="loader">
    <div class="preloader">
      <div class="spinner-layer pl-white">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div>
        <div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>
    </div>
    <p style="color:white;">Tunggu...</p>
  </div>
</div>
<?php
if(isset($_POST['submit'])){
	if($_POST['submit']=='login'){
		$username = $_POST['username'];
		$password = Password($_POST['password']);
		$login = mysql_query("select * from users where username='$username' and password='$password'") or die(mysql_error());
		$jl = mysql_num_rows($login);
		if($jl){
			$bl=mysql_fetch_array($login);
			$_SESSION['blog_login'] = $bl['ID'];
			$_SESSION['blog_nama']=$bl['user_nama'];
			$_SESSION['blog_email']=$bl['user_email'];
			$_SESSION['blog_username']=$bl['username'];
			header("location: ".getConfig('base_url'));
		}else{
			notif('<b>Login Gagal!</b> Username &amp; Password salah','danger');
		}
	}
}
?>
<div class="login-box">
  <div class="logo col-blue"> <a href="javascript:void(0);">
  <img src="images/logo.png" class="img" style="max-width:50px;"><br>
  m<b>Kasir</b></a> <small style="color:#000000 !important">Backend Management</small> </div>
  <div class="card" id="flogin">
    <div class="body">
      <form id="sign_in" method="POST">
        <input type="hidden" name="token" value="<?php echo getToken();?>">
        <div class="msg">Sign in to start your session</div>
        <div class="input-group"> <span class="input-group-addon"> <i class="material-icons">person</i> </span>
          <div class="form-line">
            <input type="text" class="form-control" name="username" placeholder="Username : demo" required autofocus>
          </div>
        </div>
        <div class="input-group"> <span class="input-group-addon"> <i class="material-icons">lock</i> </span>
          <div class="form-line">
            <input type="password" class="form-control" name="password" placeholder="Password : 123" required>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <button class="btn btn-lg btn-block bg-blue waves-effect" name="submit" value="login" type="submit">Login</button>
          </div>
        </div>
        <div class="row m-t-15">
          <div class="col-xs-12"> <a href="#" id="bforgot">Forgot Password?</a></div>
        </div>
      </form>
    </div>
  </div>
  <div class="card" id="fforgot" style="display:none">
    <div class="body">
      <form id="forgot_password" method="POST">
        <div class="msg"> Enter your email address that you used to register. We'll send you an email with your username and a
          link to reset your password. </div>
        <div class="input-group"> <span class="input-group-addon"> <i class="material-icons">email</i> </span>
          <div class="form-line">
            <input type="email" class="form-control" name="email" placeholder="Email" required autofocus>
          </div>
        </div>
        <button class="btn btn-block btn-lg bg-blue waves-effect" name="submit" value="forgot" type="submit">RESET MY PASSWORD</button>
        <div class="row m-t-20 m-b--5 align-center"> <a href="#" id="blogin">Login</a> </div>
      </form>
    </div>
  </div>
</div>

<!--
<p align="center">&copy; 2017 <a href="https://zencodev.com/">ZenCoDev</a> All rights reserved.<br>
  Developed by <a href="https://www.facebook.com/zaenalcoders/">Zaenal Abidin</a></p>
-->

<script src="<?php echo getConfig('base_url');?>plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo getConfig('base_url');?>plugins/node-waves/waves.min.js"></script>
<script src="<?php echo getConfig('base_url');?>plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?php echo getConfig('base_url');?>js/admin.js"></script>
<script>
$(document).ready(function(e) {
    $("#bforgot").click(function(e) {
        $("#flogin").hide(0,function(){
			$("#fforgot").show(0);
		});
    });
	$("#blogin").click(function(e) {
        $("#fforgot").hide(0,function(){
			$("#flogin").show(0);
		});
    });
});
</script>
</body>
</html>