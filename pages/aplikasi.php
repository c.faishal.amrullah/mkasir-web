<?php if($lihat){?>
<?php
$q=mysql_query("select * from pengguna where ID ='$lihat'") or die(mysql_error());
$b=mysql_fetch_array($q);
$online=timeago(date('Y-m-d H:i:s',$b['last_online']));
?>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>LIHAT PENGGUNA APLIKASI</h2>
      </div>
      <div class="body">
        <form>
          <div class="form-group">
            <label>Fingerprint</label>
            <div class="form-line">
              <input type="text" name="nama_kategori" value="<?php echo $b['ID'];?>" class="form-control" readonly>
            </div>
          </div>
          <div class="form-group">
            <label>Mobile</label>
            <div class="form-line">
              <input type="text" min="0" name="posisi_kategori" value="<?php echo $b['merk'];?>" class="form-control" readonly>
            </div>
          </div>
          <div class="form-group">
            <label>Os</label>
            <div class="form-line">
              <input type="text" min="0" name="posisi_kategori" value="<?php echo $b['os'];?>" class="form-control" readonly>
            </div>
          </div>
          <div class="form-group">
            <label>Operator</label>
            <div class="form-line">
              <input type="text" min="0" name="posisi_kategori" value="<?php echo $b['operator'];?>" class="form-control" readonly>
            </div>
          </div>
          <div class="form-group">
            <label>IP</label>
            <div class="form-line">
              <input type="text" min="0" name="posisi_kategori" value="<?php echo $b['ip'];?>" class="form-control" readonly>
            </div>
          </div>
          <div class="form-group">
            <label>Negara</label>
            <div class="form-line">
              <input type="text" min="0" name="posisi_kategori" value="<?php echo $b['location'];?>" class="form-control" readonly>
            </div>
          </div>
          <div class="form-group">
            <label>Online</label>
            <div class="form-line">
              <input type="text" min="0" name="posisi_kategori" value="<?php echo $online;?>" class="form-control" readonly>
            </div>
          </div>
          <div class="form-group">
            <button type="button" class="btn btn-link bg-blue waves-effect back">KEMBALI</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php }elseif($add){?>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>TAMBAH NOTIFIKASI</h2>
      </div>
      <div class="body">
        <form class="form_ajax" enctype="multipart/form-data" id="frmFileUpload">
        <input type="hidden" name="p" value="notifikasi">
        <input type="hidden" name="t" value="create">
        <input type="hidden" name="token" value="<?php echo getToken();?>">
          <div class="form-group">
            <label>Judul Posting</label>
            <div class="form-line">
              <input type="text" name="judul" class="form-control" required>
            </div>
          </div>
          <div class="form-group">
            <label>Deskripsi</label>
              <textarea name="isi" class="" id="summernote" required></textarea>
          </div>
        <div class="form-group">
          <button type="submit" class="btn btn-link bg-deep-purple waves-effect">SIMPAN</button>
          <button type="reset" class="btn btn-link bg-blue waves-effect back">BATAL</button>
        <div class="preloader pl-size-xs pull-right" style="display:none;" id="loading">
            <div class="spinner-layer pl-deep-purple">
              <div class="circle-clipper left">
                <div class="circle"></div>
              </div>
              <div class="circle-clipper right">
                <div class="circle"></div>
              </div>
            </div>
          </div>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
<?php }else{?>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>Aplikasi</h2>
      </div>
      <div class="body">
        <ul class="nav nav-tabs tab-nav-right tab-col-deep-purple" role="tablist">
          <li role="presentation" class="active"><a href="#notifikasi" data-toggle="tab" aria-expanded="true">PUSH NOTIFIKASI</a></li>
          <li role="presentation"><a href="#pengguna" data-toggle="tab" aria-expanded="true">PENGGUNA</a></li>
        </ul>
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane fade active in" id="notifikasi">
            <div class="row clearfix">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                  <div class="header">
                    <h2>KIRIM CUSTOM PUSH NOTIFIKASI</h2>
                  </div>
                  <div class="body">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Judul</th>
                            <th>Deskripsi</th>
                            <th>Diterima oleh</th>
                            <th>Dibaca oleh</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                                $no=0;
                                $q=mysql_query("select * from notifikasi order by ID desc") or die(mysql_error());
                                while($b=mysql_fetch_array($q)){
                                    $no++;
									$terima=mysql_num_rows(mysql_query("select * from notifikasi_read where ID_notifikasi='$b[ID]' and terima!=0"));
									$baca=mysql_num_rows(mysql_query("select * from notifikasi_read where ID_notifikasi='$b[ID]' and baca!=0"));
                            ?>
                          <tr>
                            <td align="center"><?php echo $no;?></td>
                            <td><?php echo $b['judul_notifikasi'];?></td>
                            <td><?php echo $b['isi_notifikasi'];?></td>
                            <td><?php echo $terima;?> pengguna</td>
                            <td><?php echo $baca;?> pengguna</td>
                            <td nowrap align="center"><button class="btn bg-red waves-effect hapus_ajax" data-id="<?php echo $b['ID'];?>" data-p="notifikasi" data-t="delete" data-token="<?php echo getToken();?>" title="Hapus"><i class="material-icons">delete</i></button></td>
                          </tr>
                          <?php
                                }
                            ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <a href="<?php echo getConfig('base_url');?>#aplikasi/add=1" class="btn btn-circle-lg bg-deep-purple waves-effect waves-circle waves-float frm btn-float" data-tooltip="true" data-placement="top" title="Tambah Data"><i class="material-icons">add</i></a>
            </div>
          <div role="tabpanel" class="tab-pane fade" id="pengguna">
            <div class="row clearfix">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                  <div class="header">
                    <h2>PENGGUNA APLIKASI</h2>
                  </div>
                  <div class="body">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Fingerprint</th>
                            <th>Mobile</th>
                            <th>OS</th>
                            <th>Negara</th>
                            <th>Operator</th>
                            <th>IP</th>
                            <th>Online</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
					$no=0;
					$q=mysql_query("select * from pengguna order by ID asc") or die(mysql_error());
					while($b=mysql_fetch_array($q)){
						$no++;
						$online=timeago(date('Y-m-d H:i:s',$b['last_online']));
				?>
                          <tr>
                            <td align="center"><?php echo $no;?></td>
                            <td><a href="<?php echo getConfig('base_url');?>#aplikasi/lihat=<?php echo $b['ID'];?>" class="btn btn-link waves-effect" title="Lihat"><?php echo $b['ID'];?></a></td>
                            <td nowrap><?php echo $b['merk'];?></td>
                            <td nowrap><?php echo $b['os'];?></td>
                            <td><?php echo $b['location'];?></td>
                            <td><?php echo $b['operator'];?></td>
                            <td><?php echo $b['ip'];?></td>
                            <td><?php echo $online;?></td>
                          </tr>
                          <?php
					}
				?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>

