<?php if($lihat){?>
<?php
$q=mysql_query("select * from member where ID ='$lihat'") or die(mysql_error());
$b=mysql_fetch_array($q);
?>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>LIHAT MEMBER</h2>
      </div>
      <div class="body">
        <form>
          <div class="input-group">
          	<label>Nama</label>
            <div class="form-line">
              <input type="text" name="nama" class="form-control" value="<?php echo $b['nama_member'];?>" placeholder="Nama" readonly>
            </div>
          </div>
          <div class="input-group">
            <label>Alamat</label>
            <div class="form-line">
              <input type="text" name="alamat" class="form-control" value="<?php echo $b['alamat_member'];?>" placeholder="Alamat" readonly>
            </div>
          </div>
          <div class="input-group">
            <label>Tempat Lahir</label>
            <div class="form-line">
              <input type="text" name="tempat_lahir" placeholder="Tempat Lahir" value="<?php echo $b['tempat_lahir_member'];?>" class="form-control" readonly>
            </div>
          </div>
          <div class="input-group">
          	<label>Tanggal Lahir</label>
            <div class="form-line">
              <input type="text" name="tgl_lahir" placeholder="Tanggal Lahir" value="<?php echo $b['tgl_lahir_member'];?>" class="form-control datepicker" readonly>
            </div>
          </div>
          <div class="input-group">
          <label>Agama</label>
            <div class="form-line">
              <select name="agama" required class="form-control" disabled>
              	<option value=""></option>
              	<option value="Islam">Islam</option>
                <option value="Kristen">Kristen</option>
                <option value="Protestan">Protestan</option>
                <option value="Hindu">Hindu</option>
                <option value="Budha">Budha</option>
                <option value="Konghucu">Konghucu</option>
                <option value="<?php echo $b['agama_member'];?>" selected><?php echo $b['agama_member'];?></option>
              </select>
            </div>
          </div>
          <div class="input-group">
          <label>No. Hp</label>
            <div class="form-line">
              <input type="tel" placeholder="No. Hp" value="<?php echo $b['no_hp_member'];?>" name="hp" class="form-control" readonly>
            </div>
          </div>
          <div class="input-group">
          <label>Email</label>
            <div class="form-line">
              <input type="email" placeholder="Email" value="<?php echo $b['email_member'];?>" name="email" class="form-control" readonly>
            </div>
          </div>
          <div class="input-group">
          <label>Username</label>
            <div class="form-line">
              <input type="text" name="username" value="<?php echo $b['username_member'];?>" placeholder="Username" class="form-control" readonly>
            </div>
          </div>
         <?php if($b['foto_member']){?>
          <div class="form-group" id="gbr">
            <img src="<?php echo $b['foto_member'];?>" width="150">
          </div>
          <?php } ?>
        <div class="footer">
          <button type="button" class="btn btn-link bg-blue waves-effect back" data-dismiss="modal">KEMBALI</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
<?php }elseif($edit){?>
<?php
$q=mysql_query("select * from member where ID ='$edit'") or die(mysql_error());
$b=mysql_fetch_array($q);
?>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>EDIT MEMBER</h2>
      </div>
      <div class="body">
        <form class="form_ajax">
        <input type="hidden" name="p" value="member">
        <input type="hidden" name="t" value="update">
        <input type="hidden" name="token" value="<?php echo getToken();?>">
        <input type="hidden" name="id" value="<?php echo $b['ID'];?>">
        <input type="hidden" name="status" value="<?php echo $b['status'];?>">
          <div class="input-group">
          	<label>Nama</label>
            <div class="form-line">
              <input type="text" name="nama" class="form-control" value="<?php echo $b['nama_member'];?>" placeholder="Nama" required>
            </div>
          </div>
          <div class="input-group">
            <label>Alamat</label>
            <div class="form-line">
              <input type="text" name="alamat" class="form-control" value="<?php echo $b['alamat_member'];?>" placeholder="Alamat" required>
            </div>
          </div>
          <div class="input-group">
            <label>Tempat Lahir</label>
            <div class="form-line">
              <input type="text" name="tempat_lahir" placeholder="Tempat Lahir" value="<?php echo $b['tempat_lahir_member'];?>" class="form-control" required>
            </div>
          </div>
          <div class="input-group">
          	<label>Tanggal Lahir</label>
            <div class="form-line">
              <input type="text" name="tgl_lahir" placeholder="Tanggal Lahir" value="<?php echo $b['tgl_lahir_member'];?>" class="form-control datepicker" required>
            </div>
          </div>
          <div class="input-group">
          <label>Agama</label>
            <div class="form-line">
              <select name="agama" required class="form-control">
              	<option value=""></option>
              	<option value="Islam">Islam</option>
                <option value="Kristen">Kristen</option>
                <option value="Protestan">Protestan</option>
                <option value="Hindu">Hindu</option>
                <option value="Budha">Budha</option>
                <option value="Konghucu">Konghucu</option>
                <option value="<?php echo $b['agama_member'];?>" selected><?php echo $b['agama_member'];?></option>
              </select>
            </div>
          </div>
          <div class="input-group">
          <label>No. Hp</label>
            <div class="form-line">
              <input type="tel" placeholder="No. Hp" value="<?php echo $b['no_hp_member'];?>" name="hp" class="form-control" required>
            </div>
          </div>
          <div class="input-group">
          <label>Email</label>
            <div class="form-line">
              <input type="email" placeholder="Email" value="<?php echo $b['email_member'];?>" name="email" class="form-control" required>
            </div>
          </div>
          <div class="input-group">
          <label>Username</label>
            <div class="form-line">
              <input type="text" name="username" value="<?php echo $b['username_member'];?>" placeholder="Username" class="form-control">
            </div>
          </div>
          <div class="input-group">
          <label>Password</label>
            <div class="form-line">
              <input type="password" name="password" placeholder="(Kosongkan jika tidak diubah)" class="form-control">
            </div>
          </div>
         <?php if($b['foto_member']){?>
          <div class="form-group" id="gbr">
            <img src="<?php echo $b['foto_member'];?>" width="150"><br><button type="button" onClick="var x = confirm('Hapus?');if(x){$('#gbr').remove();$(this).remove();$('#fdata').val('');}" class="btn bg-blue no-padding" style="padding:0px !important; margin:0 auto !important;"><i class="material-icons">clear</i></button>
          </div>
          <?php } ?>
          <div class="form-group">
                    <label>Ganti Foto</label>
                     <input name="file" id="inputFile" onChange="validateImage(this);" type="file"/>
                    <input type="hidden" name="gambar_nama" id="fname">
                    <input type="hidden" name="foto" value="<?php echo $b['foto_member'];?>" id="fdata">
            </div>
        <div class="footer">
          <div class="preloader pl-size-xs pull-right" style="display:none" id="loading">
            <div class="spinner-layer pl-deep-purple">
              <div class="circle-clipper left">
                <div class="circle"></div>
              </div>
              <div class="circle-clipper right">
                <div class="circle"></div>
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-link bg-deep-purple waves-effect">SIMPAN</button>
          <button type="reset" class="btn btn-link bg-blue waves-effect back" data-dismiss="modal">BATAL</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
<?php }else{?>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>DATA MEMBER</h2>
      </div>
      <div class="body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-hover dataTable js-exportable">
            <thead>
              <tr>
                <th>#</th>
                <th>ID</th>
                <th>Nama</th>
                <th>Hp</th>
                <th>Email</th>
                <th>Tgl Daftar</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
            	<?php
					$no=0;
					$q=mysql_query("select * from member order by ID desc") or die(mysql_error());
					while($b=mysql_fetch_array($q)){
						$no++;
				?>
                		<tr>
                        	<td align="center"><?php echo $no;?></td>
                            <td><?php echo $b['ID'];?></td>
                            <td nowrap><?php echo $b['nama_member'];?></td>
                            <td><?php echo $b['no_hp_member'];?></td>
                            <td><?php echo $b['email_member'];?></td>
                            <td nowrap><?php echo $b['tgl_daftar_member'];?></td>
                            <td nowrap align="center">
								<a href="<?php echo getConfig('base_url');?>#member/lihat=<?php echo $b['ID'];?>" class="btn bg-deep-purple waves-effect" data-tooltip="true" data-placement="top" title="Lihat"><i class="material-icons">list</i></a>
                                <a href="<?php echo getConfig('base_url');?>#member/edit=<?php echo $b['ID'];?>" class="btn bg-blue waves-effect" data-tooltip="true" data-placement="top" title="Edit"><i class="material-icons">edit</i></a>
                                <button class="btn bg-red waves-effect hapus_ajax" data-id="<?php echo $b['ID'];?>" data-p="member" data-t="delete" data-token="<?php echo getToken();?>" title="Hapus"><i class="material-icons">delete</i></button>
                            </td>
                        </tr>
                <?php
					}
				?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<button class="btn btn-circle-lg bg-deep-purple waves-effect waves-circle waves-float frm btn-float" data-toggle="modal" data-target="#defaultModal" data-controls-modal="#defaultModal" data-backdrop="static" data-keyboard="false" data-tooltip="true" data-placement="top" title="Tambah Data"><i class="material-icons">add</i></button>
<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="defaultModalLabel">Tambah User</h4>
      </div>
      <form class="form_ajax">
        <input type="hidden" name="p" value="member">
        <input type="hidden" name="t" value="create">
        <input type="hidden" name="token" value="<?php echo getToken();?>">
        <div class="modal-body">
        <div class="col-md-12">
          <div class="input-group">
            <div class="form-line">
              <input type="text" name="nama" class="form-control" placeholder="Nama" required>
            </div>
          </div>
          <div class="input-group">
            <div class="form-line">
              <input type="text" name="alamat" class="form-control" placeholder="Alamat" required>
            </div>
          </div>
          <div class="input-group">
            <div class="form-line">
              <input type="text" name="tempat_lahir" placeholder="Tempat Lahir" class="form-control" required>
            </div>
          </div>
          <div class="input-group">
            <div class="form-line">
              <input type="text" name="tgl_lahir" placeholder="Tanggal Lahir" class="form-control datepicker" required>
            </div>
          </div>
          <div class="input-group">
            <div class="form-line">
              <select name="agama" required class="form-control">
                <option value="">Agama</option>
              	<option value="Islam">Islam</option>
                <option value="Kristen">Kristen</option>
                <option value="Protestan">Protestan</option>
                <option value="Hindu">Hindu</option>
                <option value="Budha">Budha</option>
                <option value="Konghucu">Konghucu</option>
              </select>
            </div>
          </div>
          <div class="input-group">
            <div class="form-line">
              <input type="tel" placeholder="No. Hp" name="hp" class="form-control" required>
            </div>
          </div>
          <div class="input-group">
            <div class="form-line">
              <input type="email" placeholder="Email" name="email" class="form-control" required>
            </div>
          </div>
          <div class="input-group">
            <div class="form-line">
              <input type="text" name="username" placeholder="Username" class="form-control" required>
            </div>
          </div>
          <div class="input-group">
            <div class="form-line">
              <input type="password" name="password" placeholder="Password" class="form-control" required>
            </div>
          </div>
          <div class="form-line">
                    Foto <input name="file" id="inputFile" onChange="validateImage(this);" type="file"/>
                    <input type="hidden" name="gambar_nama" id="fname">
                    <input type="hidden" name="foto" id="fdata">
			</div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="preloader pl-size-xs pull-left" style="display:none" id="loading">
            <div class="spinner-layer pl-deep-purple">
              <div class="circle-clipper left">
                <div class="circle"></div>
              </div>
              <div class="circle-clipper right">
                <div class="circle"></div>
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-link bg-deep-purple waves-effect">SIMPAN</button>
          <button type="reset" class="btn btn-link bg-blue waves-effect" data-dismiss="modal">BATAL</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php } ?>
