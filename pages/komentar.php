<?php if($lihat){?>
<?php
$q=mysql_query("select b.*,a.judul_posting from posting a, komentar b where a.ID=b.ID_posting and b.ID='$lihat'") or die(mysql_error());
$b=mysql_fetch_array($q);
?>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>LIHAT KOMENTAR</h2>
      </div>
      <div class="body">
        <form>
          <div class="form-group">
            <label>Nama</label>
            <div class="form-line">
              <input type="text" name="nama_kategori" value="<?php echo $b['nama'];?>" class="form-control" readonly>
            </div>
        </div>
        <div class="form-group">
            <label>Posting</label>
            <div class="form-line">
              <input type="text" min="0" name="posisi_kategori" value="<?php echo $b['judul_posting'];?>" class="form-control" readonly>
            </div>
        </div>
        <div class="form-group">
            <label>Komentar</label>
            <div class="form-line">
              <input type="text" min="0" name="posisi_kategori" value="<?php echo $b['text_komentar'];?>" class="form-control" readonly>
            </div>
        </div>
        <div class="form-group">
            <label>Tanggal</label>
            <div class="form-line">
              <input type="text" min="0" name="posisi_kategori" value="<?php echo $b['tgl_komentar'];?>" class="form-control" readonly>
            </div>
        </div>
        <div class="form-group">
          <button type="button" class="btn btn-link bg-blue waves-effect back">KEMBALI</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
<?php }elseif($edit){?>
<?php
$q=mysql_query("select * from posting_kategori where ID ='$edit'") or die(mysql_error());
$b=mysql_fetch_array($q);
?>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>EDIT KETEGORI</h2>
      </div>
      <div class="body">
        <form class="form_ajax">
        <input type="hidden" name="p" value="kategori-posting">
        <input type="hidden" name="t" value="update">
        <input type="hidden" name="token" value="<?php echo getToken();?>">
        <input type="hidden" name="id" value="<?php echo $b['ID'];?>">
          <div class="form-group">
            <label>Nama Kategori</label>
            <div class="form-line">
              <input type="text" name="nama_kategori" value="<?php echo $b['nama_kategori'];?>" class="form-control" required>
            </div>
        </div>
        <div class="form-group">
            <label>Posisi</label>
            <div class="form-line">
              <input type="number" min="0" name="posisi_kategori" value="<?php echo $b['posisi_kategori'];?>" class="form-control" required>
            </div>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-link bg-deep-purple waves-effect">SIMPAN</button>
          <button type="reset" class="btn btn-link bg-blue waves-effect back">BATAL</button>
        <div class="preloader pl-size-xs pull-right" style="display:none;" id="loading">
            <div class="spinner-layer pl-deep-purple">
              <div class="circle-clipper left">
                <div class="circle"></div>
              </div>
              <div class="circle-clipper right">
                <div class="circle"></div>
              </div>
            </div>
          </div>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
<?php }else{?>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>KOMENTAR</h2>
      </div>
      <div class="body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-hover dataTable js-exportable">
            <thead>
              <tr>
                <th>#</th>
                <th>Nama</th>
                <th>Posting</th>
                <th>Komentar</th>
                <th>Tgl</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
            	<?php
					$no=0;
					$q=mysql_query("select b.*,a.judul_posting from posting a, komentar b where a.ID=b.ID_posting order by b.ID desc") or die(mysql_error());
					while($b=mysql_fetch_array($q)){
						$no++;
				?>
                		<tr>
                        	<td align="center"><?php echo $no;?></td>
                            <td><?php echo $b['nama'];?></td>
                            <td><?php echo $b['judul_posting'];?></td>
                            <td><?php echo $b['text_komentar'];?></td>
                            <td><?php echo $b['tgl_komentar'];?></td>
                            <td nowrap align="center">
								<a href="<?php echo getConfig('base_url');?>#komentar/lihat=<?php echo $b['ID'];?>" class="btn bg-deep-purple waves-effect" title="Lihat"><i class="material-icons">list</i></a>
                                <button class="btn bg-red waves-effect hapus_ajax" data-id="<?php echo $b['ID'];?>" data-p="komentar" data-t="delete" data-token="<?php echo getToken();?>" title="Hapus"><i class="material-icons">delete</i></button>
                            </td>
                        </tr>
                <?php
					}
				?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>
