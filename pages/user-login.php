<div class="alert alert-warning">Akun Demo! data ini tidak bisa di rubah</div>
<?php if($lihat){?>
<?php
$q=mysql_query("select * from users where ID ='$lihat'") or die(mysql_error());
$b=mysql_fetch_array($q);
?>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>LIHAT USER</h2>
      </div>
      <div class="body">
        <form>
          <div class="input-group">
            <label>Nama</label>
            <div class="form-line">
              <input type="text" name="nama" class="form-control" readonly value="<?php echo $b['user_nama']?>">
            </div>
          </div>
          <div class="input-group">
            <label>Email</label>
            <div class="form-line">
              <input type="email" name="email" class="form-control" readonly value="<?php echo $b['user_email']?>">
            </div>
          </div>
          <div class="input-group">
            <label>Username</label>
            <div class="form-line">
              <input type="text" name="username" class="form-control" readonly value="<?php echo $b['username']?>">
            </div>
          </div>
         <div class="input-group">
            <label>Tgl Daftar</label>
            <div class="form-line">
              <input type="text" name="username" class="form-control" readonly value="<?php echo $b['user_tgl']?>">
            </div>
          </div>
        <div class="form-group">
          <button type="button" class="btn btn-link bg-blue waves-effect back">KEMBALI</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
<?php }elseif($edit){?>
<?php
$q=mysql_query("select * from users where ID ='$edit'") or die(mysql_error());
$b=mysql_fetch_array($q);
?>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>EDIT USER</h2>
      </div>
      <div class="body">
        <form class="form_ajax">
        <input type="hidden" name="p" value="user-login">
        <input type="hidden" name="t" value="update">
        <input type="hidden" name="token" value="<?php echo getToken();?>">
        <input type="hidden" name="id" value="<?php echo $b['ID'];?>">
          <div class="input-group">
            <label>Nama</label>
            <div class="form-line">
              <input type="text" name="nama" class="form-control" required value="<?php echo $b['user_nama']?>">
            </div>
          </div>
          <div class="input-group">
            <label>Email</label>
            <div class="form-line">
              <input type="email" name="email" class="form-control" required value="<?php echo $b['user_email']?>">
            </div>
          </div>
          <div class="input-group">
            <label>Username</label>
            <div class="form-line">
              <input type="text" name="username" class="form-control" required value="<?php echo $b['username']?>">
            </div>
          </div>
          <div class="input-group">
            <label>Password</label>
            <div class="form-line">
              <input type="password" name="password" class="form-control" placeholder="(Kosongkan jika tidak diubah)">
            </div>
          </div>
        <div class="form-group">
          <button type="submit" class="btn btn-link bg-deep-purple waves-effect">SIMPAN</button>
          <button type="reset" class="btn btn-link bg-blue waves-effect back">BATAL</button>
        <div class="preloader pl-size-xs pull-right" style="display:none;" id="loading">
            <div class="spinner-layer pl-deep-purple">
              <div class="circle-clipper left">
                <div class="circle"></div>
              </div>
              <div class="circle-clipper right">
                <div class="circle"></div>
              </div>
            </div>
          </div>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
<?php }else{?>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>USER LOGIN</h2>
      </div>
      <div class="body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-hover dataTable js-exportable">
            <thead>
              <tr>
                <th>#</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Username</th>
                <th>Tgl Daftar</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
            	<?php
					$no=0;
					$q=mysql_query("select * from users order by ID desc") or die(mysql_error());
					while($b=mysql_fetch_array($q)){
						$no++;
				?>
                		<tr>
                        	<td align="center"><?php echo $no;?></td>
                            <td><?php echo $b['user_nama'];?></td>
                            <td><?php echo $b['user_email'];?></td>
                            <td><?php echo $b['username'];?></td>
                            <td><?php echo $b['user_tgl'];?></td>
                            <td nowrap align="center">
								<a href="<?php echo getConfig('base_url');?>#user-login/lihat=<?php echo $b['ID'];?>" class="btn bg-deep-purple waves-effect" data-tooltip="true" data-placement="top" title="Lihat"><i class="material-icons">list</i></a>
                                <a href="<?php echo getConfig('base_url');?>#user-login/edit=<?php echo $b['ID'];?>" class="btn bg-blue waves-effect" data-tooltip="true" data-placement="top" title="Edit"><i class="material-icons">edit</i></a>
                                <?
                                if($b['username'] == 'demo'){                                ?>
                                <button class="btn bg-red waves-effect hapus_ajax" data-id="<?php echo $b['ID'];?>" data-p="user-login---" data-t="delete" data-token="<?php echo getToken();?>" title="Hapus"><i class="material-icons">delete</i></button>
                                <?                                }else{                                ?>
                                <button class="btn bg-red waves-effect hapus_ajax" data-id="<?php echo $b['ID'];?>" data-p="user-login" data-t="delete" data-token="<?php echo getToken();?>" title="Hapus"><i class="material-icons">delete</i></button>
                                <?                                }
                                ?>
                            </td>
                        </tr>
                <?php
					}
				?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<button class="btn btn-circle-lg bg-deep-purple waves-effect waves-circle waves-float frm btn-float" data-toggle="modal" data-target="#defaultModal" data-controls-modal="#defaultModal" data-backdrop="static" data-keyboard="false" data-tooltip="true" data-placement="top" title="Tambah Data"><i class="material-icons">add</i></button>
<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="defaultModalLabel">Tambah User</h4>
      </div>
      <form class="form_ajax">
        <input type="hidden" name="p" value="user-login">
        <input type="hidden" name="t" value="create">
        <input type="hidden" name="token" value="<?php echo getToken();?>">
        <div class="modal-body">
          <div class="input-group">
            <label>Nama</label>
            <div class="form-line">
              <input type="text" name="nama" class="form-control" required>
            </div>
          </div>
          <div class="input-group">
            <label>Email</label>
            <div class="form-line">
              <input type="email" name="email" class="form-control" required>
            </div>
          </div>
          <div class="input-group">
            <label>Username</label>
            <div class="form-line">
              <input type="text" name="username" class="form-control" required>
            </div>
          </div>
          <div class="input-group">
            <label>Password</label>
            <div class="form-line">
              <input type="password" name="password" class="form-control" required>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="preloader pl-size-xs pull-left" style="display:none" id="loading">
            <div class="spinner-layer pl-deep-purple">
              <div class="circle-clipper left">
                <div class="circle"></div>
              </div>
              <div class="circle-clipper right">
                <div class="circle"></div>
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-link bg-deep-purple waves-effect">SIMPAN</button>
          <button type="reset" class="btn btn-link bg-blue waves-effect" data-dismiss="modal">BATAL</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php } ?>
