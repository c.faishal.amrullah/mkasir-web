<?php if($lihat){?>
<?php
$q=mysql_query("select * from produk where ID ='$lihat'") or die(mysql_error());
$b=mysql_fetch_array($q);
?>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>LIHAT PRODUK</h2>
      </div>
      <div class="body">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <?php
		  	$qg=mysql_query("select b.nama_produk,a.* from produk_gambar a, produk b where a.ID_produk=b.ID and a.ID_produk='$lihat'") or die(mysql_error());
			$jg=mysql_num_rows($qg);
		  ?>
          <ol class="carousel-indicators">
            <?php for($i=0;$i<$jg;$i++){?>
            	<li data-target="#myCarousel" data-slide-to="<?php echo $i;?>" <?php echo ($i==0) ? 'class="active"' : '';?>></li>
            <?php } ?>
          </ol>
          <div class="carousel-inner">
          <?php 
		  	$a=0;
		  	while($bg=mysql_fetch_array($qg)){?>
            	<div class="item <?php echo ($a==0) ? 'active' : '';?> text-center"> <img src="<?php echo getConfig('base_url');?>image.php?produk=<?php echo $lihat;?>" style="height:200px;" alt="<?php echo $bg['judul_posting'];?>"></div>
          <?php 
			$a++;
			}
			?>
          </div>
<a class="left carousel-control" href="#myCarousel" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> <span class="sr-only">Next</span> </a> </div>
        <form>
          <div class="form-group">
            <label>Nama Produk</label>
            <div class="form-line">
              <input type="text" name="judul_posting" class="form-control" required value="<?php echo $b['nama_produk'];?>" disabled>
            </div>
          </div>
          <div class="form-group">
            <label>Kategori</label>
            <select name="kategori" required class="form-control show-tick" data-live-search="true" disabled>
              <option value=""></option>
              <?php
					$qk=mysql_query("select * from produk_kategori order by nama_kategori asc") or die(mysql_error());
					while($bk=mysql_fetch_array($qk)){
						if($b['ID_kategori']==$bk['ID']){
				?>
              <option value="<?php echo $bk['ID'];?>" selected><?php echo $bk['nama_kategori'];?></option>
              <?php }else{ ?>
              <option value="<?php echo $bk['ID'];?>"><?php echo $bk['nama_kategori'];?></option>
              <?php } ?>
              <?php } ?>
            </select>
          </div>
          <div class="form-group" style="display:none;">
            <label>Deskripsi</label>
            <?php echo $b['deskripsi_produk'];?> </div>
          <div class="form-group">
            <label>Harga</label>
            <div class="form-line">
              <input type="number" min="1" value="<?php echo $b['harga'];?>" name="harga" class="form-control" disabled>
            </div>
          </div>
          <div class="form-group" style="display:none;">
            <label>Berat (Gram)</label>
            <div class="form-line">
              <input type="number" min="1" value="<?php echo $b['berat'];?>" name="berat" class="form-control" disabled>
            </div>
          </div>
          <div class="form-group">
            <label>Stok</label>
            <div class="form-line">
              <input type="number" min="1" value="<?php echo $b['stok'];?>" name="stok" class="form-control" disabled>
            </div>
          </div>
          <div class="form-group">
            <button type="reset" class="btn btn-link bg-blue waves-effect back">KEMBALI</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php }elseif($edit){?>
<?php
$q=mysql_query("select * from produk where ID ='$edit'") or die(mysql_error());
$b=mysql_fetch_array($q);
?>
<link href="<?php echo getConfig('base_url');?>plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo getConfig('base_url');?>plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css">
<link rel="stylesheet" type="text/css" href="<?php echo getConfig('base_url');?>plugins/summernote/summernote.css">
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>EDIT PRODUK</h2>
      </div>
      <div class="body">
        <form class="form_ajax" enctype="multipart/form-data" id="frmFileUpload">
          <input type="hidden" name="p" value="posting">
          <input type="hidden" name="t" value="update">
          <input type="hidden" name="token" value="<?php echo getToken();?>">
          <input type="hidden" name="id" value="<?php echo $b['ID'];?>">
          <div class="form-group">
            <label>Gambar</label>
            <div class="form-line">
              <div class="file-loading">
                <input id="gambar" name="gambar[]" type="file" multiple>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>Nama Produk</label>
            <div class="form-line">
              <input type="text" name="judul_posting" class="form-control" required value="<?php echo $b['nama_produk'];?>">
            </div>
          </div>
          <div class="form-group">
            <label>Kategori</label>
            <select name="kategori" required class="form-control show-tick" data-live-search="true" data-header="Pilih Kategori">
              <option value=""></option>
              <?php
					$qk=mysql_query("select * from produk_kategori order by nama_kategori asc") or die(mysql_error());
					while($bk=mysql_fetch_array($qk)){
						if($b['ID_kategori']==$bk['ID']){
				?>
              <option value="<?php echo $bk['ID'];?>" selected><?php echo $bk['nama_kategori'];?></option>
              <?php }else{ ?>
              <option value="<?php echo $bk['ID'];?>"><?php echo $bk['nama_kategori'];?></option>
              <?php } ?>
              <?php } ?>
            </select>
          </div>
          <div class="form-group" style="display:none">
            <label>Deskripsi</label>
            <textarea name="deskripsi_posting" class="" id="summernote" required><?php echo $b['deskripsi_produk'];?></textarea>
          </div>
          <div class="form-group">
            <label>Harga</label>
            <div class="form-line">
              <input type="number" min="1" value="<?php echo $b['harga'];?>" name="harga" class="form-control" required>
            </div>
          </div>
          <div class="form-group" style="display:none;">
            <label>Berat (Gram)</label>
            <div class="form-line">
              <input type="number" min="1" value="<?php echo $b['berat'];?>" name="berat" class="form-control" required>
            </div>
          </div>
          <div class="form-group">
            <label>Stok</label>
            <div class="form-line">
              <input type="number" min="1" value="<?php echo $b['stok'];?>" name="stok" class="form-control" required>
            </div>
          </div>
          <div class="form-group" style="display:none;">
            <label>Tgl Posting</label>
            <div class="form-line">
              <input type="text" name="tgl_posting" value="<?php echo $b['tgl_posting'];?>" class="form-control datepicker" required>
            </div>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-link bg-deep-purple waves-effect">SIMPAN</button>
            <button type="reset" class="btn btn-link bg-blue waves-effect back">BATAL</button>
            <div class="preloader pl-size-xs pull-right" style="display:none;" id="loading">
              <div class="spinner-layer pl-deep-purple">
                <div class="circle-clipper left">
                  <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                  <div class="circle"></div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php
$g=mysql_query("select * from produk_gambar where ID_produk='$edit'");
$url;
$i=0;
$pr;
while($bg=mysql_fetch_array($g)){
	$i++;
	$url.="'".getConfig('base_url').'image.php?produk='.$edit."',";
	$pr.='{caption: "'.$bg['gambar'].'", downloadUrl: "'.getConfig('base_url').'image.php?produk='.$edit.'", key: '.$bg['ID'].'},';
}
$url = substr($url,0,(strlen($url)-1));
$pr = substr($pr,0,(strlen($pr)-1));
?>
<script>
$(function(){
	$("#gambar").fileinput({
        initialPreview: [<?php echo $url;?>],
        initialPreviewAsData: true,
        deleteUrl: "processData.php?p=upload&t=delete",
        overwriteInitial: false,
		initialPreviewConfig: [<?php echo $pr;?>],
        uploadUrl: "processData.php?p=upload&t=create",
        maxFileCount: 1,
		showUpload :true,
		required:false,
		allowedFileTypes:['image'],
		uploadAsync: false,
		uploadExtraData: function() {
        	return {upload:'img',t:'create',key:'<?php echo $edit;?>',token:'<?php echo getToken();?>'};
		}
    });
});
</script>
<?php }elseif($add){?>
<style>
.kv-file-upload{
	display:none !important;
}
</style>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>TAMBAH PRODUK BARU</h2>
      </div>
      <div class="body">
        <form enctype="multipart/form-data" id="frmFileUpload">
          <input type="hidden" name="p" value="posting">
          <input type="hidden" name="t" value="create">
          <input type="hidden" name="token" value="<?php echo getToken();?>">
          <input type="hidden" name="id" value="<?php echo $b['ID'];?>">
          <div class="form-group">
            <label>Gambar</label>
            <div class="form-line">
              <div class="file-loading">
                <input id="gambar" name="gambar[]" type="file" multiple required>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>Nama Produk</label>
            <div class="form-line">
              <input type="text" name="judul_posting" class="form-control" required>
            </div>
          </div>
          <div class="form-group">
            <label>Kategori</label>
            <select name="kategori" required class="form-control show-tick" data-live-search="true" data-header="Pilih Kategori">
              <option value=""></option>
              <?php
					$q=mysql_query("select * from produk_kategori order by nama_kategori asc") or die(mysql_error());
					while($b=mysql_fetch_array($q)){
				?>
              <option value="<?php echo $b['ID'];?>"><?php echo $b['nama_kategori'];?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group" style="display:none;">
            <label>Deskripsi</label>
            <textarea name="deskripsi_posting" class="" id="summernote" required>1</textarea>
          </div>
          <div class="form-group">
            <label>Harga</label>
            <div class="form-line">
              <input type="number" min="1" name="harga" class="form-control" required>
            </div>
          </div>
          <div class="form-group" style="display:none">
            <label>Berat (Gram)</label>
            <div class="form-line">
              <input type="number" min="1" value="1" name="berat" class="form-control" required>
            </div>
          </div>
          <div class="form-group">
            <label>Stok</label>
            <div class="form-line">
              <input type="number" min="1" name="stok" class="form-control" required>
            </div>
          </div>
          <div class="form-group" style="display:none">
            <label>Tgl Posting</label>
            <div class="form-line">
              <input type="text" name="tgl_posting" value="<?php echo date('Y-m-d');?>" class="form-control datepicker" required>
            </div>
          </div>
          <div class="form-group">
            <button type="submit" id="spn" class="btn btn-link bg-deep-purple waves-effect">SIMPAN</button>
            <button type="reset" class="btn btn-link bg-blue waves-effect back">BATAL</button>
            <div class="preloader pl-size-xs pull-right" style="display:none;" id="loading">
              <div class="spinner-layer pl-deep-purple">
                <div class="circle-clipper left">
                  <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                  <div class="circle"></div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
$(function(){
	$("#gambar").fileinput({
        uploadUrl: "processData.php?p=upload&t=create",
        maxFileCount: 1,
		showUpload :false,
		required:true,
		allowedFileTypes:['image'],
		uploadAsync: false,
		uploadExtraData: function() {
        	return {upload:'img',t:'create',token:'<?php echo getToken();?>'};
		}
    });
	$("#frmFileUpload").submit(function(e) {
        e.preventDefault();
		$("#frmFileUpload #loading").show();
		$("#gambar").fileinput('upload');
		var a=$(this).serializeObject();console.log(a);
		$("#frmFileUpload *").attr("disabled","true");
		$('#gambar').on('filebatchuploadsuccess', function(event, data, previewId, index) {
			var form = data.form, files = data.files, extra = data.extra, response = data.response, reader = data.reader;
			if(response.uploaded=='OK'){
				processAjax(a,"Data berhasil disimpan");	
			}
		});
    });
});
</script>
<?php }else{?>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>PRODUK</h2>
      </div>
      <div class="body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-hover dataTable js-exportable">
            <thead>
              <tr>
                <th>#</th>
                <th>Nama Produk</th>
                <th>Kategori</th>
                <th>Harga</th>
                <th>Stock</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php
					$no=0;
					$q=mysql_query("select a.ID as ID_POST,a.*,b.* from produk a,produk_kategori b where a.ID_kategori=b.ID order by a.ID desc") or die(mysql_error());
					while($b=mysql_fetch_array($q)){
						$no++;
				?>
              <tr>
                <td align="center"><?php echo $no;?></td>
                <td><?php echo $b['nama_produk'];?></td>
                <td><?php echo $b['nama_kategori'];?></td>
                <td><?php echo $b['harga'];?></td>
                <td><?php echo $b['stok'];?></td>
                <td nowrap align="center"><a href="<?php echo getConfig('base_url');?>#posting/lihat=<?php echo $b['ID_POST'];?>" class="btn bg-deep-purple waves-effect" title="Lihat"><i class="material-icons">list</i></a> <a href="<?php echo getConfig('base_url');?>#posting/edit=<?php echo $b['ID_POST'];?>" class="btn bg-blue waves-effect" title="Edit"><i class="material-icons">edit</i></a>
                  <button class="btn bg-red waves-effect hapus_ajax" data-id="<?php echo $b['ID_POST'];?>" data-p="posting" data-t="delete" data-token="<?php echo getToken();?>" title="Hapus"><i class="material-icons">delete</i></button></td>
              </tr>
              <?php
					}
				?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<a href="<?php echo getConfig('base_url');?>#posting/add=1" class="btn btn-circle-lg bg-deep-purple waves-effect waves-circle waves-float frm btn-float" data-tooltip="true" data-placement="top" title="Tambah Data"><i class="material-icons">add</i></a>
<?php } ?>
