-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 13 Des 2017 pada 12.45
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+07:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_mkasir`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `config`
--

CREATE TABLE `config` (
  `ID` int(11) NOT NULL,
  `config_key` text NOT NULL,
  `config_value` longtext NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `config`
--

INSERT INTO `config` (`ID`, `config_key`, `config_value`, `keterangan`) VALUES
(1, 'base_url', 'http://localhost/pos/server/', 'Base URL Server');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE `produk` (
  `ID` int(11) NOT NULL,
  `ID_kategori` int(11) NOT NULL,
  `nama_produk` longtext NOT NULL,
  `deskripsi_produk` longtext NOT NULL,
  `tgl_posting` varchar(32) NOT NULL,
  `harga` int(11) NOT NULL,
  `berat` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `posted_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `hit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`ID`, `ID_kategori`, `nama_produk`, `deskripsi_produk`, `tgl_posting`, `harga`, `berat`, `stok`, `posted_by`, `status`, `hit`) VALUES
(33, 26, 'Makanan 1', '1', '2017-12-09', 25000, 10, 100, 14, 1, 0),
(34, 24, 'Kosmetik 1', '1', '2017-12-09', 30000, 10, 16, 14, 1, 4),
(35, 23, 'Snack 1', '1', '2017-12-09', 3000, 1, 199, 14, 1, 1),
(36, 26, 'Makanan 2', '1', '2017-12-09', 20000, 10, 99, 14, 1, 1),
(37, 25, 'Minuman 1', '1', '2017-12-09', 5000, 10, 100, 14, 1, 0),
(38, 26, 'Makanan 3', '1', '2017-12-09', 12000, 1, 99, 14, 1, 1),
(39, 23, 'Snack 2', '1', '2017-12-09', 2000, 1, 100, 14, 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk_gambar`
--

CREATE TABLE `produk_gambar` (
  `ID` int(11) NOT NULL,
  `ID_produk` varchar(32) NOT NULL,
  `gambar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk_gambar`
--

INSERT INTO `produk_gambar` (`ID`, `ID_produk`, `gambar`) VALUES
(46, '29', '1652711025_44642_L_1.jpg'),
(57, '33', '2097836581_Facts_on_seafood_and_fish_allergies_original_resized.jpg'),
(58, '34', '216574369_65._Kosmetik.jpg'),
(59, '35', '1662797307_EL_MiniChips_12oz_12ct_SnackPack_Uncropped.png'),
(60, '36', '1893644996_foto_5_makanan_ini_justru_bisa_menimbulkan_kelelahan.jpg'),
(61, '37', '951404463_ice_drink_4.jpg'),
(62, '38', '1106331603_daging asap.jpg'),
(63, '39', '337410982_f9700db1_f695_436e_853a_c760c8e89238.jpg.w480.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk_kategori`
--

CREATE TABLE `produk_kategori` (
  `ID` int(11) NOT NULL,
  `nama_kategori` varchar(64) NOT NULL,
  `posisi_kategori` int(11) NOT NULL,
  `tgl_buat` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk_kategori`
--

INSERT INTO `produk_kategori` (`ID`, `nama_kategori`, `posisi_kategori`, `tgl_buat`) VALUES
(23, 'Snack', 0, '2017-10-22 14:19:50'),
(24, 'Kosmetik', 1, '2017-10-25 00:58:18'),
(25, 'Minuman', 2, '2017-10-25 00:58:24'),
(26, 'Makanan', 0, '2017-10-25 00:58:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id_` int(11) NOT NULL,
  `ID` int(11) NOT NULL,
  `subtotal` varchar(32) NOT NULL,
  `diskon` varchar(32) NOT NULL,
  `grandtotal` varchar(32) NOT NULL,
  `bayar` varchar(32) NOT NULL,
  `kembali` varchar(32) NOT NULL,
  `waktu` varchar(32) NOT NULL,
  `status` varchar(32) NOT NULL,
  `itemdata` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id_`, `ID`, `subtotal`, `diskon`, `grandtotal`, `bayar`, `kembali`, `waktu`, `status`, `itemdata`) VALUES
(24, 791217, '20,000', '0', '20000', '20,000', '0,0', '2017-12-09 02:53:54', 'Sukses', '{\"bcart\":[{\"ID\":\"36\",\"nama\":\"Makanan 2\",\"harga\":\"20,000\",\"qty\":\"1\",\"jml\":\"20,000\"}]}'),
(25, 176940, '30,000', '10', '27000', '50,000', '23,000', '2017-12-09 02:54:42', 'Sukses', '{\"bcart\":[{\"ID\":\"34\",\"nama\":\"Kosmetik 1\",\"harga\":\"30,000\",\"qty\":\"1\",\"jml\":\"30,000\"}]}'),
(26, 539496, '3,000', '0', '3000', '0,0', '0,0', '2017-12-09 02:55:26', 'Batal', '{\"bcart\":[{\"ID\":\"35\",\"nama\":\"Snack 1\",\"harga\":\"3,000\",\"qty\":\"1\",\"jml\":\"3,000\"}]}'),
(27, 927021, '30,000', '0', '30000', '0,0', '0,0', '2017-12-09 02:56:13', 'Batal', '{\"bcart\":[{\"ID\":\"34\",\"nama\":\"Kosmetik 1\",\"harga\":\"30,000\",\"qty\":\"1\",\"jml\":\"30,000\"}]}'),
(28, 609942, '30,000', '15', '25500', '25,500', '0,0', '2017-12-09 02:58:29', 'Sukses', '{\"bcart\":[{\"ID\":\"34\",\"nama\":\"Kosmetik 1\",\"harga\":\"30,000\",\"qty\":\"1\",\"jml\":\"30,000\"}]}'),
(29, 718152, '30,000', '0', '30000', '0,0', '0,0', '2017-12-09 02:59:47', 'Batal', '{\"bcart\":[{\"ID\":\"34\",\"nama\":\"Kosmetik 1\",\"harga\":\"30,000\",\"qty\":\"1\",\"jml\":\"30,000\"}]}'),
(30, 557676, '12,000', '0', '12000', '0,0', '0,0', '2017-12-09 03:00:28', 'Batal', '{\"bcart\":[{\"ID\":\"38\",\"nama\":\"Makanan 3\",\"harga\":\"12,000\",\"qty\":\"1\",\"jml\":\"12,000\"}]}');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_detail`
--

CREATE TABLE `transaksi_detail` (
  `ID` int(11) NOT NULL,
  `ID_transaksi` varchar(32) NOT NULL,
  `ID_produk` int(11) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi_detail`
--

INSERT INTO `transaksi_detail` (`ID`, `ID_transaksi`, `ID_produk`, `qty`) VALUES
(8, '791217', 36, 1),
(9, '176940', 34, 1),
(10, '539496', 35, 1),
(11, '927021', 34, 1),
(12, '609942', 34, 1),
(13, '718152', 34, 1),
(14, '557676', 38, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `ID` int(11) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(64) NOT NULL,
  `user_nama` varchar(32) NOT NULL,
  `user_email` varchar(32) NOT NULL,
  `user_tgl` varchar(32) NOT NULL,
  `user_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`ID`, `username`, `password`, `user_nama`, `user_email`, `user_tgl`, `user_status`) VALUES
(14, 'demo', 'ebaba2d20b0f0ed1070b22a5c8d4928e996b3e3a177df99c770e40b44ef1b578', 'Demo Administrator', 'info@zencodev.com', '2017-01-01 00:00:00', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `produk_gambar`
--
ALTER TABLE `produk_gambar`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `produk_kategori`
--
ALTER TABLE `produk_kategori`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_`);

--
-- Indexes for table `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `produk_gambar`
--
ALTER TABLE `produk_gambar`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `produk_kategori`
--
ALTER TABLE `produk_kategori`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
