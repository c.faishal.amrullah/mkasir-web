<?php
session_start();
require_once('inc/dbcon.php' );
require_once( 'inc/function.php' );
require_once( 'inc/config.php' );
ob_start( "ob_html_compress" );
if ( $_SESSION[ 'blog_login' ] ) {
	require_once( 'main.php' );
} else {
	require_once( 'pages/login.php' );
}
ob_end_flush();
mysql_close( $connect );
?>